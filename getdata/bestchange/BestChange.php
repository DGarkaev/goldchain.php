<?php

namespace BestChange;

use Exception;
use BestChange\Exception\BestChangeException;
use DateTime;
use DateTimeZone;


require_once 'bm_cy.php';
require_once 'bm_bcodes.php';
require_once 'bm_top.php';
require_once 'bm_rates.php';
require_once 'bm_exch.php';

require_once 'connect2db.php';

class BestChange
{
    private $version = '';
    /** @var DateTime */
    private $lastUpdate;

    const PREFIX_TMPFILE = 'nbc';
    const BESTCHANGE_FILE = 'http://www.bestchange.ru/bm/info.zip';

    const FILE_BM_EXCH = 'bm_exch.dat';
    const FILE_BM_RATES = 'bm_rates.dat';
    const FILE_BM_CY = 'bm_cy.dat';
    const FILE_BM_BCODES = 'bm_bcodes.dat';
    const FILE_BM_TOP = 'bm_top.dat';


    const TIMEOUT = 15;

    /** @var \ZipArchive */
    private $zip;

    private $bm_cy;
    private $bm_bcodes;
    private $bm_rates;
    private $bm_top;
    private $bm_exch;

    private $cachePath;
    private $useCache;
    private $cacheTime;

    /**
     * BestChange constructor.
     * @param string $cachePath
     * @param int $cacheTime
     * @throws BestChangeException
     */
    public function __construct($cachePath = '', $cacheTime = 3600)
    {
        $this->zip = new \ZipArchive();
        if ($cachePath) {
            $this->cacheTime = $cacheTime;
            $this->useCache = true;
            $this->cachePath = $cachePath;
        } else {
            $this->useCache = false;
            $this->cachePath = tempnam(sys_get_temp_dir(), self::PREFIX_TMPFILE);
        }
        register_shutdown_function([$this, 'close']); // уборка мусора
        $this->load();
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Завершаем работу. Убираем мусор
     */
    public function close()
    {
        if (!$this->useCache) {
            if (!is_writable($this->cachePath)) {
                chmod($this->cachePath, 0644);
            }
            unlink($this->cachePath);
        }
        return $this;
    }

    /**
     * @return $this
     * @throws BestChangeException
     */
    private function load()
    {
        $this->getFile()->unzip()->init();
        $this->bm_bcodes = new bm_bcodes($this->zip->getFromName(self::FILE_BM_BCODES));
        $this->bm_rates = new bm_rates($this->zip->getFromName(self::FILE_BM_RATES));
        $this->bm_cy = new bm_cy($this->zip->getFromName(self::FILE_BM_CY));
        $this->bm_top = new bm_top($this->zip->getFromName(self::FILE_BM_TOP));
        $this->bm_exch = new bm_exch($this->zip->getFromName(self::FILE_BM_EXCH));
        return $this;
    }

    /**
     * @return $this
     * @throws BestChangeException
     */
    private function getFile()
    {
        if ($this->useCacheFile()) {
            return $this;
        }
        $file = $this->loadFile(self::BESTCHANGE_FILE);
        if ($file) {
            $fp = fopen($this->cachePath, 'wb+');
            fputs($fp, $file);
            fclose($fp);
            return $this;
        }
        throw new BestChangeException('Файл на bestchange.ru не найден или недоступен');
    }

    private function useCacheFile()
    {
        clearstatcache(true, $this->cachePath);
        return (
            $this->useCache
            && file_exists($this->cachePath)
            && filemtime($this->cachePath) > (time() - $this->cacheTime)
        );
    }

    /**
     * @return $this
     * @throws BestChangeException
     */
    private function unzip()
    {
        if (!$this->zip->open($this->cachePath)) {
            throw new BestChangeException('Получен битый файл с bestchange.ru');
        }
        return $this;
    }

    private function init()
    {
        $file = explode("\n", $this->zip->getFromName('bm_info.dat'));
        foreach ($file as $row) {
            $row = iconv('CP1251', 'UTF-8', $row);
            $data = array_map('trim', explode('=', $row));
            if (count($data) < 2) {
                continue;
            }
            switch ($data[0]) {
                case'last_update':
                    $this->lastUpdate = $this->canonicalDate($data[1]);
                    break;
                case'current_version':
                    $this->version = $data[1];
                    break;
            }
        }
        return $this;
    }

    private function canonicalDate($date)
    {
        $arMonth = [
            'января' => 'January',
            'февраля' => 'February',
            'марта' => 'March',
            'апреля' => 'April',
            'мая' => 'May',
            'июня' => 'June',
            'июля' => 'July',
            'августа' => 'August',
            'сентября' => 'September',
            'октября' => 'October',
            'ноября' => 'November',
            'декабря' => 'December',
        ];
        foreach ($arMonth as $ru => $en) {
            $date = preg_replace('/' . $ru . '/sui', $en, $date);
        }
        $dt = new DateTime($date, new DateTimeZone('Asia/Omsk'));
        $dt->modify('+3 hour');
        return $dt;//, new DateTimeZone('Europe/Moscow'));
    }

    private function loadFile($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, self::TIMEOUT);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    /**
     * @param $link
     * @throws Exception
     */
    private function lastupdateDB($link)
    {
        $strt = date_format($this->lastUpdate, 'Y-m-d H:i:s');
        $sql = "INSERT INTO lastupdate (id,datetime) 
                    VALUES ('1', '$strt') 
                    ON DUPLICATE KEY UPDATE datetime = '$strt'";
        if (mysqli_query($link, $sql) === FALSE) {
            throw new BestChangeException(sql_error($link));
        }
    }


    public function upgateDB()
    {
        $link = db_connect();

        if (!$link) {
            echo db_connect_error($link);
            return;
        }
        $link->set_charset('utf8');

        mysqli_begin_transaction($link);
        try
        {
            $this->lastupdateDB($link);
            $this->bm_cy->updateDB($link);
            $this->bm_top->updateDB($link);
            $this->bm_bcodes->updateDB($link);
            $this->bm_exch->updateDB($link);
            $this->bm_rates->updateDB($link);
            mysqli_commit($link);
        }
        catch (Exception $e)
        {
            mysqli_rollback($link);
        }
        mysqli_close($link);
    }
}