<?php
/**
 * Created by PhpStorm.
 * User: dgarkaev
 * Date: 22.10.18
 * Time: 11:16
 */

namespace BestChange;

/*
 * 0 - id
 * 1 - name
 * 2 - long name
 */

use BestChange\Exception\BestChangeException;

class bm_bcodes
{
    private $data = [];

    public function __construct($data)
    {
        $data = iconv('CP1251', 'UTF-8', $data);
        $this->data = explode("\n", $data);
    }

    /**
     * @param $link
     * @throws \Exception
     */
    public function updateDB($link)
    {
        if (mysqli_query($link, 'DELETE FROM bm_bcodes') === FALSE) {
            throw new BestChangeException('[bm_bcodes]:'.sql_error($link));
        }
        foreach ($this->data as $r) {
            $f=explode(';', $r);
            $sql = "INSERT INTO bm_bcodes (id, name, lname) 
                    VALUES ('$f[0]', '$f[1]', '$f[2]')";
            if (mysqli_query($link, $sql) === FALSE) {
                throw new BestChangeException(sql_error($link));
            }
        }
    }
}