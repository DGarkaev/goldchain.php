<?php
/**
 * Created by PhpStorm.
 * User: dgarkaev
 * Date: 22.10.18
 * Time: 11:16
 */

namespace BestChange;

/*
 * 0 - id_out
 * 1 - id_inp
 * 2 - id_exch
 * 3 - rate_out
 * 4 - rate_inp
 * 5 - reserve
 */

use BestChange\Exception\BestChangeException;

class bm_rates
{
    private $data = [];

    public function __construct($data)
    {
        //$data = iconv('CP1251', 'UTF-8', $data);
        $this->data = explode("\n", $data);
    }

    /**
     * @param $link
     * @throws \Exception
     */
    public function updateDB($link)
    {
        if (mysqli_query($link, 'DELETE FROM bm_rates') === FALSE) {
            throw new \Exception('[bm_rates]:'.sql_error($link));
        }
        foreach ($this->data as $r) {
            $f = explode(';', $r);
            $sql = "INSERT INTO bm_rates (id_out,id_inp,id_exch,rate_out,rate_inp,reserve) 
                    VALUES ('$f[0]', '$f[1]', '$f[2]', '$f[3]', '$f[4]', '$f[5]')";
            if (mysqli_query($link, $sql) === FALSE) {
                throw new BestChangeException('[bm_rates]:'.sql_error($link));
            }
        }
    }
}