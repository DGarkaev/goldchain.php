<?php
/**
 * Created by PhpStorm.
 * User: dgarkaev
 * Date: 22.10.18
 * Time: 11:16
 */

namespace BestChange;

/*
 * 0 - id
 * 1 -
 * 2 - name
 * 3 -
 * 4 - id cyrrency*/

use BestChange\Exception\BestChangeException;

class bm_cy
{
    private $data = [];

    public function __construct($data)
    {
        $data = iconv('CP1251', 'UTF-8', $data);
        $this->data = explode("\n", $data);
    }

    /**
     * @param $link
     * @throws \Exception
     */
    public function updateDB($link)
    {
        // bm_cy
        foreach ($this->data as $r) {
            $f = explode(';', $r);
            $sql = "INSERT INTO bm_cy (id, name, id_currency, id_type) 
                    VALUES ('$f[0]', '$f[2]', '$f[4]', '$f[5]') 
                    ON DUPLICATE KEY UPDATE name='$f[2]', id_currency='$f[4]', id_type='$f[5]'";
            if (mysqli_query($link, $sql) === FALSE) {
                throw new BestChangeException('[bm_cy]:'.sql_error($link));
            }
        }

    }
}