<?php

namespace BestChange;


function db_connect()
{
    $db_host = '127.0.0.1';
    $db_user = 'goldchain';
    $db_pass = '123';
    $db_database = 'goldchain';

    $link = mysqli_connect($db_host, $db_user, $db_pass, $db_database);
    return $link;
}

function db_connect_error($link)
{
    if (!$link) {
        $err = array('error' => true,
            'errno' => mysqli_connect_errno(),
            'error_text' => mysqli_connect_error());
        $err = json_encode($err, JSON_UNESCAPED_UNICODE);
        return $err;
    }
    return "{}";
}

function sql_error($link)
{
    $err = array('error' => true,
        'error_text' => mysqli_error($link));
    $err = json_encode($err, JSON_UNESCAPED_UNICODE);
    return $err;
}

?>