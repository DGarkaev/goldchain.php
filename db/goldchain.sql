-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `goldchain`;
CREATE DATABASE `goldchain` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `goldchain`;

DROP TABLE IF EXISTS `bm_bcodes`;
CREATE TABLE `bm_bcodes` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Индекс 2` (`id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `bm_brates`;
CREATE TABLE `bm_brates` (
  `id1` int(11) DEFAULT NULL,
  `id2` int(11) DEFAULT NULL,
  `rates` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `bm_currency_type`;
CREATE TABLE `bm_currency_type` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE `bm_currency_type`;
INSERT INTO `bm_currency_type` (`id`, `name`) VALUES
(0,	'электронная валюта'),
(1,	'интернет-банкинг'),
(2,	'денежные переводы'),
(3,	'наличные деньги');

DROP TABLE IF EXISTS `bm_cy`;
CREATE TABLE `bm_cy` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `id_currency` int(11) DEFAULT NULL,
  `id_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Индекс 2` (`id`,`name`),
  KEY `Индекс 3` (`name`),
  KEY `id_currency` (`id_currency`),
  KEY `id_currency_id_type` (`id_currency`,`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `bm_exch`;
CREATE TABLE `bm_exch` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Индекс 2` (`id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP VIEW IF EXISTS `bm_exch_url`;
CREATE TABLE `bm_exch_url` (`id` int(11), `name` varchar(45), `url` varchar(50));


DROP TABLE IF EXISTS `bm_rates`;
CREATE TABLE `bm_rates` (
  `id_out` int(11) DEFAULT NULL,
  `id_inp` int(11) DEFAULT NULL,
  `id_exch` int(11) DEFAULT NULL,
  `rate_out` double DEFAULT NULL,
  `rate_inp` double DEFAULT NULL,
  `reserve` double DEFAULT NULL,
  `rate` double GENERATED ALWAYS AS (`rate_inp` / `rate_out`) VIRTUAL,
  KEY `Индекс 4` (`id_out`,`id_inp`),
  KEY `Индекс 5` (`id_out`),
  KEY `Индекс 6` (`id_inp`),
  KEY `Индекс 7` (`id_exch`),
  KEY `Индекс 8` (`id_inp`,`id_out`),
  CONSTRAINT `bm_rates_ibfk_1` FOREIGN KEY (`id_exch`) REFERENCES `bm_exch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bm_rates_ibfk_2` FOREIGN KEY (`id_inp`) REFERENCES `bm_cy` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bm_rates_ibfk_3` FOREIGN KEY (`id_out`) REFERENCES `bm_cy` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP VIEW IF EXISTS `bm_rates_all`;
CREATE TABLE `bm_rates_all` (`id_out` int(11), `id_inp` int(11), `id_exch` int(11), `rate` double, `reserve` double, `id_cout` int(11), `id_cinp` int(11), `out` varchar(45), `inp` varchar(45), `exch` varchar(45));


DROP TABLE IF EXISTS `bm_top`;
CREATE TABLE `bm_top` (
  `id1` int(11) DEFAULT NULL,
  `id2` int(11) DEFAULT NULL,
  `rates` double DEFAULT NULL,
  KEY `FK_bm_top_bm_cy` (`id1`),
  KEY `FK_bm_top_bm_cy_2` (`id2`),
  CONSTRAINT `FK_bm_top_bm_cy` FOREIGN KEY (`id1`) REFERENCES `bm_cy` (`id`),
  CONSTRAINT `FK_bm_top_bm_cy_2` FOREIGN KEY (`id2`) REFERENCES `bm_cy` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `dn_rates`;
CREATE TABLE `dn_rates` (
  `exch` varchar(50) DEFAULT NULL,
  `c_out` varchar(50) DEFAULT NULL,
  `c_inp` varchar(50) DEFAULT NULL,
  `buy` double DEFAULT NULL,
  `sell` double DEFAULT NULL,
  KEY `Индекс 1` (`exch`,`c_out`,`c_inp`),
  KEY `Индекс 2` (`exch`,`c_out`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP VIEW IF EXISTS `dn_rates_btc`;
CREATE TABLE `dn_rates_btc` (`exch` varchar(50), `c_out` varchar(50), `c_inp` varchar(50), `buy` double, `sell` double, `id_out` int(11), `id_inp` int(11));


DROP TABLE IF EXISTS `lastupdate`;
CREATE TABLE `lastupdate` (
  `id` int(11) NOT NULL,
  `datetime` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `bm_exch_url`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `bm_exch_url` AS select `bm_exch`.`id` AS `id`,`bm_exch`.`name` AS `name`,concat('https://www.bestchange.ru/click.php?id=',`bm_exch`.`id`) AS `url` from `bm_exch`;

DROP TABLE IF EXISTS `bm_rates_all`;
CREATE ALGORITHM=UNDEFINED DEFINER=`goldchain`@`localhost` SQL SECURITY DEFINER VIEW `bm_rates_all` AS select `a`.`id_out` AS `id_out`,`a`.`id_inp` AS `id_inp`,`a`.`id_exch` AS `id_exch`,`a`.`rate_inp` / `a`.`rate_out` AS `rate`,`a`.`reserve` AS `reserve`,`b`.`id_currency` AS `id_cout`,`c`.`id_currency` AS `id_cinp`,`b`.`name` AS `out`,`c`.`name` AS `inp`,`d`.`name` AS `exch` from (((`bm_rates` `a` left join `bm_cy` `b` on(`a`.`id_out` = `b`.`id`)) left join `bm_cy` `c` on(`a`.`id_inp` = `c`.`id`)) left join `bm_exch` `d` on(`a`.`id_exch` = `d`.`id`)) order by `a`.`id_out`,`a`.`id_inp`,`a`.`rate_inp` / `a`.`rate_out` desc;

DROP TABLE IF EXISTS `dn_rates_btc`;
CREATE ALGORITHM=UNDEFINED DEFINER=`goldchain`@`localhost` SQL SECURITY DEFINER VIEW `dn_rates_btc` AS select `a`.`exch` AS `exch`,`a`.`c_out` AS `c_out`,`a`.`c_inp` AS `c_inp`,`a`.`buy` AS `buy`,`a`.`sell` AS `sell`,(select `bm_cy`.`id` from `bm_cy` where `a`.`c_out` = 'btc' and `bm_cy`.`name` like '%(btc)') AS `id_out`,(select `bm_cy`.`id` from `bm_cy` where `a`.`c_out` = 'btc' and `bm_cy`.`name` like concat('%',`a`.`exch`,'%') and `bm_cy`.`name` like concat('%',`a`.`c_inp`,'%')) AS `id_inp` from `dn_rates` `a`;

-- 2018-10-24 04:53:13
