<?php
require_once('connect2db.php');

$link = db_connect();

if (!$link) {
    echo db_connect_error($link);
    exit;
}
$link->set_charset('utf8');

$sql = mysqli_query($link, 'select id, name from bm_cy order by id');

$data = array();
while ($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)) {
    $data[] = $row;
}
$data = json_encode($data, JSON_UNESCAPED_UNICODE);
header_remove();
http_response_code(200);
header('Content-Type: application/json;charset=utf-8');
header('Status: ' . '200');
print_r($data);
mysqli_close($link);
?>

