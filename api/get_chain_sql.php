<?php

function get_chain1_sql($id_out, $id_inp, $top, $reserve, $limit, $cex, $eex)
{
return
"with W as (select B.* from
(
select 
row_number() 
over (PARTITION BY id_out, id_inp ORDER BY rate desc) as num
, A.* 
, R1.name as name_out
, R1.id_currency as id_currency_out
, R1.id_type as id_type_out
, R2.name as name_inp
, R2.id_currency as id_currency_inp
, R2.id_type as id_type_inp
, R3.name as name_exch
, R3.url as url
from
bm_rates as A
left join bm_cy as R1 on (R1.id = A.id_out)
left join bm_cy as R2 on (R2.id = A.id_inp)
left join bm_exch_url as R3 on (R3.id = A.id_exch)
where not A.reserve < $reserve and id_out <> id_inp and id_inp not in ($cex) and id_exch not in ($eex)
order by id_out, id_inp, rate desc
) B
where not B.num > $top 
)

select 
C.rate as rate,
C.rate as rate1,
C.id_out as id_out1,
C.id_currency_out as id_currency_out1,
C.id_type_out as id_type_out1,

C.id_inp as id_inp1,
C.id_currency_inp as id_currency_inp1,
C.id_type_inp as id_type_inp1

,C.id_exch as id_exch1
,C.reserve as reserve1
,C.name_out as out1
,C.name_inp as inp1
,C.name_exch as exch1
,concat(C.url,'&from=',C.id_out,'&to=',C.id_inp,'&url=1') as exch_url
from
W as C
where 
C.id_out = $id_out 
and C.id_inp in ($id_inp)  
order by 1 desc limit $limit";
}

function get_chain2_sql($id_out, $id_inp, $top, $reserve, $limit, $cex, $eex)
{
return "
-- EXPLAIN
with W as (select B.* from
		(
		select 
		-- row_number() 
		 rank()
		over (PARTITION BY id_out, id_inp ORDER BY rate desc) as num
		, A.* 
		, R1.name as name_out
		, R1.id_currency as id_currency_out
		, R1.id_type as id_type_out
		, R2.name as name_inp
		, R2.id_currency as id_currency_inp
		, R2.id_type as id_type_inp
		, R3.name as name_exch
		, R3.url as url
		from
		bm_rates as A
		left join bm_cy as R1 on (R1.id = A.id_out)
		left join bm_cy as R2 on (R2.id = A.id_inp)
		left join bm_exch_url as R3 on (R3.id = A.id_exch)
		where not A.reserve < $reserve and id_out <> id_inp and id_inp not in ($cex) and id_exch not in ($eex)
		order by id_out, id_inp, rate desc
		) B
    where not B.num > $top 
    )

select 
(C1.rate*C2.rate) as rate,

C2.rate as rate1,
C2.id_out as id_out1,
C2.id_currency_out as id_currency_out1,
C2.id_type_out as id_type_out1,

C2.id_inp as id_inp1,
C2.id_currency_inp as id_currency_inp1,
C2.id_type_inp as id_type_inp1,

C2.id_exch as id_exch1,
C2.reserve as reserve1,
C2.name_out as out1,
C2.name_inp as inp1,
C2.name_exch as exch1,
concat(C2.url,'&from=',C2.id_out,'&to=',C2.id_inp,'&url=1') as exch_url1,


C1.rate as rate2,
C1.id_out as id_out2,
C1.id_currency_out as id_currency_out2,
C1.id_type_out as id_type_out2,

C1.id_inp as id_inp2,
C1.id_currency_inp as id_currency_inp2,
C1.id_type_inp as id_type_inp2,

C1.id_exch as id_exch2,
C1.reserve as reserve2,
C1.name_out as out2,
C1.name_inp as inp2,
C1.name_exch as exch2,
concat(C1.url,'&from=',C1.id_out,'&to=',C1.id_inp,'&url=1') as exch_url2

from
	W as C1,
	W as C2
where 
C2.id_out = $id_out
and C2.id_inp=C1.id_out
and  C1.id_inp in ($id_inp)

order by 1 desc limit $limit";
}

function get_chain3_sql($id_out, $id_inp, $top, $reserve, $limit, $cex, $eex)
{
    return
        "
		with W as (select B.* from
				(
				select 
				-- row_number() 
				 rank()
				over (PARTITION BY id_out, id_inp ORDER BY rate desc) as num
				, A.* 
				, R1.name as name_out
				, R1.id_currency as id_currency_out
				, R1.id_type as id_type_out
				, R2.name as name_inp
				, R2.id_currency as id_currency_inp
				, R2.id_type as id_type_inp
				, R3.name as name_exch
				, R3.url as url
				from
				bm_rates as A
				left join bm_cy as R1 on (R1.id = A.id_out)
				left join bm_cy as R2 on (R2.id = A.id_inp)
				left join bm_exch_url as R3 on (R3.id = A.id_exch)
				where not A.reserve < $reserve and id_out <> id_inp and id_inp not in ($cex)  and id_exch not in ($eex)
				order by id_out, id_inp, rate desc
				) B
			where not B.num > $top 
			)
			
		select 
			(C1.rate*C2.rate*C3.rate) as rate,
			
		
		C3.rate as rate1,
		C3.id_out as id_out1,
		C3.id_currency_out as id_currency_out1,
		C3.id_type_out as id_type_out1,
		
		C3.id_inp as id_inp1,
		C3.id_currency_inp as id_currency_inp1,
		C3.id_type_inp as id_type_inp1,
		
		C3.id_exch as id_exch1,
		C3.reserve as reserve1,
		C3.name_out as out1,
		C3.name_inp as inp1,
		C3.name_exch as exch1,
		concat(C3.url,'&from=',C3.id_out,'&to=',C3.id_inp,'&url=1') as exch_url1,
		
		C2.rate as rate2,
		C2.id_out as id_out2,
		C2.id_currency_out as id_currency_out2,
		C2.id_type_out as id_type_out2,
		
		C2.id_inp as id_inp2,
		C2.id_currency_inp as id_currency_inp2,
		C2.id_type_inp as id_type_inp2,
		
		C2.id_exch as id_exch2,
		C2.reserve as reserve2,
		C2.name_out as out2,
		C2.name_inp as inp2,
		C2.name_exch as exch2,
		concat(C2.url,'&from=',C2.id_out,'&to=',C2.id_inp,'&url=1') as exch_url2,
		
		
		C1.rate as rate3,
		C1.id_out as id_out3,
		C1.id_currency_out as id_currency_out3,
		C1.id_type_out as id_type_out3,
		
		C1.id_inp as id_inp3,
		C1.id_currency_inp as id_currency_inp3,
		C1.id_type_inp as id_type_inp3,
		
		C1.id_exch as id_exch3,
		C1.reserve as reserve3,
		C1.name_out as out3,
		C1.name_inp as inp3,
		C1.name_exch as exch3,
		concat(C1.url,'&from=',C1.id_out,'&to=',C1.id_inp,'&url=1') as exch_url3	
		from
			W as C1,
			W as C2,
			W as C3
		where 
		C1.id_inp in ($id_inp)
		and C2.id_inp=C1.id_out 
		and C3.id_inp=C2.id_out 
		and C3.id_out = $id_out
		order by 1 desc limit $limit";
}
