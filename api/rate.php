<?php
//самые выгодные курсы
require_once 'connect2db.php';
require_once 'get_chain_sql.php';

header_remove();
http_response_code(200);
header('Content-Type: application/json;charset=utf-8');
header('Access-Control-Allow-Origin: *');
header('Status: ' . '200');

$type = "desc";

$chain="1";
if (isset($_GET['chain'])) {
    $chain = $_GET['chain'];
} else {
    $err = array('error' => true,
        'error_text' => "Не задан параметр 'chain'");
    $err = json_encode($err, JSON_UNESCAPED_UNICODE);
    echo $err;
    exit;
}
if ($chain > 3 || $chain < 1) {
    $err = array('error' => true,
        'error_text' => "Значения параметра 'chain' выходят за диапазон допустимых значений [1-3]");
    $err = json_encode($err, JSON_UNESCAPED_UNICODE);
    echo $err;
    exit;
}

$top = 1;
if (isset($_GET['top'])) {
    $top = $_GET['top'];
}

$limit = 3;
if (isset($_GET['limit'])) {
    $limit = $_GET['limit'];
}

$id_cex = 0;
if (isset($_GET['cex'])) {
    $id_cex = $_GET['cex'];
} 

$id_eex = 0;
if (isset($_GET['eex'])) {
    $id_eex = $_GET['eex'];
} 

$id_out = "";
if (isset($_GET['out'])) {
    $id_out = $_GET['out'];
    $id_out = explode(",", $id_out);
    if ($id_out == false) {
        $err = array('error' => true,
            'error_text' => "Ошибка в задании параметра 'out'");
        $err = json_encode($err, JSON_UNESCAPED_UNICODE);
        echo $err;
        exit;
    }
} else {
    $err = array('error' => true,
        'error_text' => "Не задан параметр 'out'");
    $err = json_encode($err, JSON_UNESCAPED_UNICODE);
    echo $err;
    exit;
}

# Проверим размерность inp - она должна быть одинаковой,
# для этого БД должна быть уже открыта
$link = db_connect();

if (!$link) {
    echo db_connect_error($link);
    exit;
}
$link->set_charset('utf8');

$id_inp = "";
if (isset($_GET['inp'])) {
    $id_inp = $_GET['inp'];
    #$id_inp = "C.id_inp in ($id_inp)";
} else {
    $err = array('error' => true,
        'error_text' => "Не задан параметр 'inp'");
    $err = json_encode($err, JSON_UNESCAPED_UNICODE);
    echo $err;
    exit;
}
# проверим на одинаковый код валюты в INP
$sql_str = "select id_currency from bm_cy where id in (" . $_GET['inp'] . ") group by id_currency";
$sql = mysqli_query($link, $sql_str);

if (!$sql) {
    $err = array('error' => true,
        'error_text' => mysqli_error($link));
    $err = json_encode($err, JSON_UNESCAPED_UNICODE);
    echo $err;
    exit;
}

$num_rows = mysqli_num_rows($sql);
if ($num_rows != 1) {
    $err = array('error' => true,
        'error_text' => "Не одинаковый код валюты в параметре 'inp'");
    $err = json_encode($err, JSON_UNESCAPED_UNICODE);
    echo $err;
    exit;
}

$reserve = 0;
if (isset($_GET['reserve'])) {
    $reserve = $_GET['reserve'];
}

$rzdata = array();
foreach ($id_out as $i) {
    $data = array();
    $f = 'get_chain'.$chain.'_sql';
    $sql_str = $f($i, $id_inp, $top, $reserve, $limit, $id_cex, $id_eex);

    $sql = mysqli_query($link, $sql_str);

    if (!$sql) {
        $err = array('error' => true,
            'error_text' => mysqli_error($link));
        $err = json_encode($err, JSON_UNESCAPED_UNICODE);
        echo $err;
        exit;
    }
    while ($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)) {
        $data[] = $row;
    }
    $rzdata[$i] = $data;
}
$json = json_encode($rzdata, JSON_UNESCAPED_UNICODE);
echo $json;
mysqli_close($link);
