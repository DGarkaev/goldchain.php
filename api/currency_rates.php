<?php
require_once 'connect2db.php';

header_remove();
http_response_code(200);
header('Content-Type: application/json;charset=utf-8');
header('Access-Control-Allow-Origin: *');
header('crossDomain: true');
header('Status: ' . '200');

$link = db_connect();

if (!$link) {
    echo db_connect_error($link);
    exit;
}

$sql_str= "select * from bm_brates_ext";
$sql = mysqli_query($link, $sql_str);

if (!$sql) {
    $err = array('error' => true,
        'error_text' => mysqli_error($link));
    $err = json_encode($err, JSON_UNESCAPED_UNICODE);
    echo $err;
    exit;
}

while ($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)) {
    $data[] = $row;
}

$json = json_encode($data, JSON_UNESCAPED_UNICODE);
echo $json;
mysqli_close($link);
?>